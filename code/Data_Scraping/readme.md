# Newspaper Scraping Project

This python project is a newspaper scraping project which extracts the relevant information from the newspaper and stores it into a mongo collection. Additionally, there is a functionality to write into a csv file. 

## Author Sudeshna Bora 

## Explanation 

The main code is in convertor.py . 
The convertor.py depends upon english_config.json and german_config.json to get relevant html tag information for scrapping. 
As an end user, please change line # 16 (language = 'english' or language = 'german') to get the relevant information for scraping. 
The mongo database name is <b>covid_data</b> and the collection containing the scrapped data is <b>details</b> . Additionally, we have another collection <b>newspaper</b> that contains the html file. 
Once the data is scrapped, the newpaper is shifted to a folder called scrapped. This step is taken so that the new crawled newspaper gets written there. On running this job again only the new newspapers will be scrapped.

## Python Dependency

To run this python project we can install the dependencies using the requirements.txt

<code>
pip install -r requirements.txt
</code>

## Mongo Commands 

The most relevant mongo command here is to export the mongo data 

<code>
mongoexport --host localhost --db covid_data --collection details --type=csv --out english.csv --fields paper_name,date,heading,data,link --query '{"language":{"$eq":'english'}}'
</code>

Some other used mongo commands are 

1. To use a database
<code>
use [database_name]
</code>

2. To find the number of entries in a collection. This should be done usually after the above command (use database_name)
<code>
db.[collection_name].count()
</code>

3. To print all the entries.

<code>
db.[collection_name].find().pretty()
</code>

### Notes

In case you have updated the code (basically introducting new dependencies). You should update the requirements.txt file. 
This can be done as by running <code>pip freeze > requirements.txt </code> 

Again, if you want to install new module , it can be done as <code> pip install [module_name]</code>. To uninstall, this can be done by <code>pip uninstall [module_name]</code>
