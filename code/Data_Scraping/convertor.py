# importing the libraries
from bs4 import BeautifulSoup
import os
from os import listdir
from os.path import isfile, join
import codecs
import csv
import re
import json
from glob import glob
import shutil
import pymongo
import sys

# give which paper you want to scrape
language="english"
mypath="news"
configFile=language+"_config.json"

destination='/home/cloud/news/scrapped/'

fieldnames = ['paper_name','date','heading','data','link', 'comments']

paths = glob(mypath+'/*/')
# db connection
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["covid_data"]
mycol = mydb["details"]

rowColumn = []
myData = []

def readConfig(paper_name):
    '''
    Reads the config file to get the paper. This gives which element to be scanned
    '''
    with open(configFile) as f:
        data = json.load(f)
    return data.get(paper_name)

def write_dataFile(paper_data):
    '''
    Function to write the scrapped data into a csv file
    '''

    # html_file.replace('.html','.csv')
    with open(language+'_dataDump.csv', mode='a') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        for heading, data in paper_data.items():
            heading=heading.split('__')
            paper_name=heading[1]
            date_name=heading[2]
            heading=heading[0]
            data=data.split('__')
            link=data[1]
            data=data[0]
            writer.writerow({'paper_name':paper_name.encode('utf-8'),'date':date_name,'heading':heading,'data':data.encode('utf-8'),'link':link,'html_content':html_content })


def extract_dataFile(soup,element,class_name,paper_name,date_name):
    '''
     Function to extract the important information from the html file
    '''
    data_array={}
    if class_name=='':
        coverpage_news = soup.find_all(element)
    else:
        coverpage_news = soup.find_all(element, class_=class_name)
	# Empty lists for content, links and titles
    for n in range(len(coverpage_news)):
        if coverpage_news[n].find('a') is not None and "href" in str(coverpage_news[n].find('a')):
            link=coverpage_news[n].find('a')["href"]
            title = coverpage_news[n].find('a').get_text()
        else:
            link=''
            if coverpage_news[n].find('span') is not None:
                title = coverpage_news[n].find('span').get_text()
            else:
                title = coverpage_news[n].get_text()
        if( len(title.split())>3 ):
            data_array.update({element+'__'+paper_name+'__'+date_name+'__'+str(n) : re.sub(r"[\n\r\t]+", ' ', title)+'__'+link})
    return data_array

def filter_duplicate_data(data):
    result = {}
    for key,value in data.items():
        if value not in result.values():
            result[key] = value
    return result

tempPaperData=[]
newsPaperData = []
for path in paths:
    html_files = [f for f in listdir(path) if isfile(join(path, f))]
    merged_data = {}
    print(html_files)
    for html_file in html_files:
        if html_file == '.DS_Store':
            continue
        paper_name=html_file.split('_')[-1].replace('.html','')
        date_name=html_file.split('_')[0]

        # for html_file in html_files:
        try:
            file = codecs.open(path+html_file, "r", "utf-8")
            html_content=file.read()
            file.close()
	    # Parse the html content
            soup = BeautifulSoup(html_content, "lxml")
            tempData=[]
            for data in readConfig(paper_name):
                for html_tag, classname in data.items():
                    tempData.append(extract_dataFile(soup,html_tag,classname,paper_name,date_name))
                    newsPaperData.append({'paperId':html_file,'html_content':html_content})
                    tempPaperData.append({i:j for x in tempData for i,j in x.items()})
                    #dest = shutil.move(path+html_file, destination+path)
            print(path + html_file)
            try:
                shutil.move(path+html_file,destination+path)
            except IOError as io_err:
                print("Not moved " + str(io_err))
                os.makedirs(os.path.dirname(destination + path))
                shutil.move(path+ html_file, destination + path)

        except Exception as e:
            print("The newspaper is not parsed: "+ paper_name+"because of e: "+ str(e))
            break

merged_data={i:j for x in tempPaperData for i,j in x.items()}
# write_dataFile(filter_duplicate_data(merged_data))
for heading, data in filter_duplicate_data(merged_data).items():
    heading=heading.split('__')
    paper_name=heading[1]
    date_name=heading[2]
    heading=heading[0]
    data=data.split('__')
    link=data[1]
    data=data[0]
    myData.append({'paper_name':paper_name,'date':date_name,'heading':heading,'data':data,'link':link, 'language': language})

if len(myData) > 0:
    x = mycol.insert_many(myData)
    mycol = mydb["newspaper"]
    y = mycol.insert_many(newsPaperData)
